let upper = 1000
let numArray = [...Array(upper).keys()]
let nats = numArray.reduce((acc, num, i) => {
    if (i % 3 === 0 || i % 5 === 0) {
        return acc.concat(i)
    }
    return acc
}, [])
let sum = nats.reduce((acc, num) => acc + num, 0)


console.log('Answer:', sum)