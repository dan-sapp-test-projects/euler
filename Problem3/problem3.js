let upper = 600851475143
let numArray = [...Array(upper).keys()]
let listOfPrimes = numArray.reduce((acc, i) => {
    let subArray = [...Array(upper).keys()]
    let isPrime = true
    subArray.forEach(number => {
        if (number !== 1 && number !== 0 && number !== i && i % number === 0) {
            isPrime = false
        }
    })
    if (isPrime) {
        return acc.concat(i)
    }
    return acc
}, [])

console.log('lastOfPrimes', listOfPrimes[listOfPrimes.length - 1])